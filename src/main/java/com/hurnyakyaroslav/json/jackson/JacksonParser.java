package com.hurnyakyaroslav.json.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hurnyakyaroslav.model.Device;
import sun.jvm.hotspot.runtime.ObjectMonitor;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
    private ObjectMapper objectMapper;

    public JacksonParser() {
        objectMapper = new ObjectMapper();
    }
public List<Device> getDeviceList(File json){
List<Device> devices = null;
    try {
        devices.addAll( Arrays.asList(objectMapper.readValue(json, Device[].class)));
    } catch (IOException e) {
        e.printStackTrace();
    }

    return devices;
}
}
