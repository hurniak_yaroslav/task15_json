package com.hurnyakyaroslav;

import com.google.gson.internal.$Gson$Preconditions;
import com.hurnyakyaroslav.json.gson.GSONParser;
import com.hurnyakyaroslav.json.jackson.JacksonParser;
import com.hurnyakyaroslav.json.validators.FirstValidator;
import com.hurnyakyaroslav.model.Device;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        File json = new File("src/main/resources/json/devices.json");
        File schema = new File("src/main/resources/json/devicesSchema.json");
        if (!FirstValidator.validate(json, schema)) {
            System.out.println("Validation failed(");
            //   return;
        }
        GSONParser gsonParser = new GSONParser();
        List<Device> devices = null;
        try {
            devices = gsonParser.getDevices(json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        displayValues(devices);

        JacksonParser jacksonParser = new JacksonParser();
        devices = jacksonParser.getDeviceList(json);
        displayValues(devices);

    }

    public static void displayValues(List objects) {
        objects.stream().forEach(el -> System.out.println(el));
    }
}
