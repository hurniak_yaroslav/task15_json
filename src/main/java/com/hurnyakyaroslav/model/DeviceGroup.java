package com.hurnyakyaroslav.model;

public enum DeviceGroup {
    INPUT, OUTPUT, MULTIMEDIA
}
